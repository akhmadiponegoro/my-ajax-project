from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout
import requests
import json

def book_func(request):
    return render(request, 'library.html')

def jsonreq_func(request, book):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + book)
    return JsonResponse(response.json())